import React from 'react';
import { Image } from 'react-native'

export default class LogoTitle extends React.Component {
  render() {
    return (
      <Image
        source={require('../assets/logo.png')}
        style={{ marginLeft: 20, width: 80, height: 30 }}
      />
    );
  }
}