import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, TextInput, Image, Alert, Button} from 'react-native';
import axios from 'axios';
import LogoTitle from "./Dashboard";

export default class Login extends React.Component {

  state = {
    email: '',
    password: ''
  }

  login = () => { // inizio login
    if (this.state.email.length > 0 && this.state.password.length > 6) {
       axios.post('http://progetto5.dev.sviluppo.tech/api/v1/auth/login', {
         email: this.state.email,
         password: this.state.password
       })
         .then((response) => {
           this.props.navigation.navigate('Dashboard')
           // cambio di schermata
         })
         .catch((error) => {
           Alert.alert('Errore', 'Dati inseriti non validi');
         })
    } else {
      Alert.alert('Errore', 'Compila i campi');
    }

  } // fine login

  next = () => {
    this._passwordInput.focus();
  }

  render() {
    return (
      <View style={styles.container}>

        <Image style={{marginTop: 40}} source={require('../assets/logo.png')}/>

        <View style={styles.form_input}>

          <Text style={styles.intestazione}>
            Email
          </Text>

          <TextInput
            style={styles.input}
            onChangeText={(email) => this.setState({email})}
            value={this.state.email}
            autoCapitalize="none"
            autoCorrect={false}
            keyboardType="email-address"
            returnKeyType="next"
            onSubmitEditing={this.next}
            blurOnSubmit={true} />

        </View>

        <View style={styles.form_input}>

          <Text style={styles.intestazione}>
            Password
          </Text>

          <TextInput
            style={styles.input}
            onChangeText={(password) => this.setState({password})}
            ref={ref => {this._passwordInput = ref}}
            value={this.state.password}
            autoCapitalize="none"
            autoCorrect={false}
            keyboardType="default"
            returnKeyType="done"
            blurOnSubmit={true}
            secureTextEntry={true} />
        </View>

        <TouchableOpacity
          style={styles.bottone_accedi}
          onPress={this.login}
          activeOpacity={0.8}>

          <Text style={styles.textButton}> ACCEDI </Text>

        </TouchableOpacity>


      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20
  },
  form_input: {
    marginTop:50
  },
  intestazione: {
    color: '#141414',
  },
  input: {
    padding: 10,
    color: '#959595',
    fontSize: 16,
    borderBottomWidth: 1,
    borderBottomColor: '#00A4FF'
  },
  bottone_accedi: {
    marginTop:70,
    height:50,
    backgroundColor: '#00A4FF',
    justifyContent: 'center',
  },
  textButton: {
    justifyContent: 'center',
    alignSelf: 'center',
    color: 'white',
  },
})