import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, ActivityIndicator, Button} from 'react-native';
import ChartView from 'react-native-highcharts';
import LogoTitle from "./Logo";
import axios from 'axios';

export default class Dashboard extends React.Component {

  static navigationOptions = ({navigation}) => {
    return {
      headerTitle: <LogoTitle/>,
      headerRight: (
        <Button
          style={{marginRight: 20}}
          onPress={() => navigation.navigate('Login')}
          title="LOGOUT"
          color="#00A4FF"
        />
      ),
    }
  };

  state = {
    dati: {},
    isLoading: true
  }

  async componentDidMount() {

    this.carica_dati();
  }

  carica_dati = async () => {
    axios.get('https://api.openweathermap.org/data/2.5/weather?q=rome&units=metric&appid=2ce9c27bad816b51eb6943db06f0383f')
      .then((response) => {
        this.setState({
          dati: response.data.main,
          isLoading: false
        })
      })
  }

  render() {

    const options = {
      title: 'titolo',
      series: [{
        type: 'pie',
        name: 'Browser share',
        data: [
          ['Firefox', 45.0],
          ['IE', 26.8],
          {
            name: 'Chrome',
            y: 12.8,
            sliced: true,
            selected: true
          },

          ['Safari', 8.5],
          ['Opera', 6.2],
          ['Others', 0.7]
        ]
      }]
    };

    if (this.state.isLoading) {
      return (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'white'}}>
          <ActivityIndicator size={'large'} color={'00A4FF'}/>
          <Text style={styles.title}>CARICAMENTO DATI</Text>
        </View>
      )
    }

    return (
      <View style={styles.container}>

        <View style={{flex: 0.5}}>

          <ChartView style={{height: 300}} config={options}></ChartView>

        </View>

        <View style={{flex: 0.5, padding: 30}}>

          <Text style={{fontSize: 20}}>TEMPERATURA: {Math.round(this.state.dati.temp)} °C</Text>

          <Text style={{fontSize: 20}}>TEMPERATURA MASSIMA: {Math.round(this.state.dati.temp_max)} °C</Text>

          <Text style={{fontSize: 20}}>TEMPERATURA MINIMA: {Math.round(this.state.dati.temp_min)} °C</Text>


        </View>


      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center'
  },
})