import {createStackNavigator, createAppContainer} from 'react-navigation';

import Login from './Login'
import Dashboard from './Dashboard'

const RootNavigator = createStackNavigator({
    Login: {screen: Login, navigationOptions: { header: null}},
    Dashboard: {screen: Dashboard},
  }, {
    initialRouteName: 'Login',
  }
);

export default createAppContainer(RootNavigator);