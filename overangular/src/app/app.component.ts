import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent { // inizio classe

  controllo = false;

  constructor(){ // inizio costruttore
  } // fine costruttore

  ngOnInit() {
    if (localStorage.getItem('overtoken_angular') === null) {
      this.controllo = false
    } else {
      this.controllo = true
    }
  }

}
// fine classe
