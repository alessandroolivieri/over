import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit { // inizio classe

  title = 'overangular';
  temperatura  = {};
  math = Math;
  Highcharts = Highcharts;
  mostra = false;

  chartOptions = {
    title: {
      text: '',
    },
    series: [{
      type: 'pie',
      name: 'Browser share',
      data: [
        ['Firefox',   45.0],
        ['IE',       26.8],
        {
          name: 'Chrome',
          y: 12.8,
          sliced: true,
          selected: true
        },

        ['Safari',    8.5],
        ['Opera',     6.2],
        ['Others',   0.7]
      ]
    }]
  };


  constructor(private httpClient: HttpClient,   private router: Router){ // inizio costruttore
  } // fine costruttore

  ngOnInit () { // inizio ngOnInit
    this.httpClient.get('https://api.openweathermap.org/data/2.5/weather?q=rome&units=metric&appid=2ce9c27bad816b51eb6943db06f0383f').subscribe((res)=>{
      console.log(res);
      this.temperatura = res;
      this.mostra = true;
    });
  } // fine ngOnInit

  logout () { // inizio logout
    localStorage.removeItem('overtoken_angular');
    location.reload();
  } // fine logout

} // fine classe
