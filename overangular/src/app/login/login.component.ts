import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  email: string = '';
  password: string = '';

  constructor(private httpClient: HttpClient) { // inizio costruttore
  }

  login () { // inizio login
    if (this.email.length > 0 && this.password.length > 6) {
      this.httpClient.post('http://progetto5.dev.sviluppo.tech/api/v1/auth/login', {
        email: this.email,
        password: this.password
      }).subscribe(
        data => {
          localStorage.setItem('overtoken_angular', data['token']);
          location.reload();
        },
        error => {
          console.error(error.error)
          alert('Errore, controlla i dati inseriti')
        }
      );
    } else {
      alert('compila i campi');
    }
  }

} // fine classe
